program CodingSample;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  System.SysUtils,
  System.RegularExpressions,
  System.RegularExpressionsCore;

var
  regExString: string;
  inputString: string;
  regEx: TRegEx;
  regExOptions: TRegExOptions;
  matches: TMatchCollection;
  match: TMatch;
  matchCount: Integer;
  group: TGroup;
  groupIx: Integer;

begin
  try
    while True do
    begin
      Writeln('Enter the RegEx or enter to exit');
      Readln(regExString);
      if regExString = '' then
        break;

      try
        regExOptions := [roIgnoreCase];
        regEx := TRegEx.Create(regExString, regExOptions);
        regEx.Match('a');

        Writeln('Enter the string to check');
        Readln(inputString);
        Writeln;

        matches := regEx.matches(inputString);
        if (matches.Count = 0) then
        begin
          Writeln('** No Matches **' + sLineBreak + sLineBreak);
          continue;
        end;

        matchCount := 0;
        for match in matches do
        begin
          inc(matchCount);
          Writeln(Format('Match %d: Offset: %d Length: %d Value: "%s"',
            [matchCount, match.Index, match.Length, match.Value]));

          groupIx := 1;
          while groupIx < match.Groups.Count do
          begin
            group := match.Groups[groupIx];
            Writeln(Format('  Match group %d: offset: %d length: %d value: %s',
              [groupIx, group.Index, group.Length, group.Value]));
            inc(groupIx);
          end;
          Writeln;
        end;
        Writeln;
      except
        on E: ERegularExpressionError do
        begin
          Writeln('That does not appear to be a valid regular expression!' +
            sLineBreak);
        end;
      end;
    end;

  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;

(*
  Why I don't use RegEx in the Delphi IDE to replace!
   roIgnoreCase, roMultiLine, roExplicitCapture,

  ro([A-Z][a-zA-Z]+),
  \tcbx$1.Tag := Ord(ro$1);\n

  However, copying to Notepad++ just the section you want to manipulate
  and copying it back is much safer in case you make a mistake with your
  search and end up changing things that you didn't expect to (or can see)
*)

end.
