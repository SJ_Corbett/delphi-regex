unit formMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  System.RegularExpressions,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.TMSScrollControl, FMX.TMSRichEditorBase, FMX.TMSRichEditor,
  FMX.TMSBaseControl, FMX.TMSMemo, FMX.Controls.Presentation, FMX.StdCtrls,
  FMX.ScrollBox, FMX.Memo;

type
  TForm1 = class(TForm)
    reRegEx: TTMSFMXRichEditor;
    Label1: TLabel;
    Label2: TLabel;
    Timer1: TTimer;
    lblRegExErr: TLabel;
    Timer2: TTimer;
    reResults: TTMSFMXRichEditor;
    Label3: TLabel;
    Memo1: TMemo;
    mmoMatch: TMemo;
    GroupBox1: TGroupBox;
    cbxIgnoreCase: TCheckBox;
    cbxMultiLine: TCheckBox;
    cbxExplicitCapture: TCheckBox;
    cbxSingleLine: TCheckBox;
    cbxIgnorePatternSpace: TCheckBox;
    cbxNotEmpty: TCheckBox;
    cbxCompiled: TCheckBox;
    procedure Timer1Timer(Sender: TObject);
    procedure OnRegExChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mmoMatchTextChange(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
  private
    RegEx: TRegEx;
    Processing: Boolean;
    procedure ProcessRegExMatch;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.UIConsts, System.RegularExpressionsCore;

const
  cnstPlain = claBlack;
  cnstMatch = claBlue;
  cnstCaptured = claRed;

procedure TForm1.FormCreate(Sender: TObject);
begin
  ReportMemoryLeaksOnShutdown := true;
  Processing := False;

  cbxIgnoreCase.Tag := Ord(roIgnoreCase);
  cbxMultiLine.Tag := Ord(roMultiLine);
  cbxExplicitCapture.Tag := Ord(roExplicitCapture);
  cbxSingleLine.Tag := Ord(roSingleLine);
  cbxIgnorePatternSpace.Tag := Ord(roIgnorePatternSpace);
  cbxNotEmpty.Tag := Ord(roNotEmpty);
  cbxCompiled.Tag := Ord(roCompiled);

  cbxMultiLine.Hint :=
    'Changes the meaning of ^ and $ so they match at the beginning' + sLineBreak
    + 'and end, respectively, of any line, and not just the beginning' +
    sLineBreak + 'and end of the entire string.';
  cbxExplicitCapture.Hint :=
    'Specifies that the only valid captures are explicitly named or' +
    sLineBreak + 'numbered groups of the form (?<name>�). This allows unnamed' +
    sLineBreak +
    'parentheses to act as noncapturing groups without the syntactic' +
    sLineBreak + 'clumsiness of the expression (?:�).';
  cbxSingleLine.Hint :=
    'Specifies single-line mode. Changes the meaning of the dot (.) so' +
    sLineBreak +
    'it matches every character (instead of every character except \n).';
  cbxIgnorePatternSpace.Hint :=
    'Eliminates unescaped white space from the pattern and enables comments' +
    sLineBreak +
    'marked with #. However, the IgnorePatternWhitespace value does not' +
    sLineBreak + 'affect or eliminate white space in character classes.';
  cbxCompiled.Hint :=
    ' Specifies that the regular expression is compiled to an assembly. This' +
    sLineBreak + 'yields faster execution but increases startup time.';
end;

procedure TForm1.OnRegExChange(Sender: TObject);
begin
  Timer1.Enabled := false;
  Timer1.Enabled := true;
end;

procedure TForm1.ProcessRegExMatch;
var
  wkText: string;
  matches: TMatchCollection;
  ix: Integer;
  match: TMatch;
  matchCount: Integer;
  group: TGroup;
  groupIx: Integer;

  resultArray: array of Integer;
  currentColour: Integer;
  outString: string;
begin
  if Processing or lblRegExErr.Visible then
    exit;

  Processing := true;
  try
    wkText := mmoMatch.Text;
    reResults.Clear;
    Memo1.Lines.Clear;
    if (wkText = '') or (Trim(reRegEx.Text) = '') then
    begin
      reResults.AddMultiLineText(wkText);
      exit;
    end;
    matches := RegEx.matches(wkText);
    if (matches.Count = 0) then
    begin
      Memo1.Lines.Add('** No Matches **');
      reResults.AddMultiLineText(wkText);
      exit;
    end;

    SetLength(resultArray, wkText.Length);
    for ix := Low(resultArray) to High(resultArray) do
      resultArray[ix] := cnstPlain;

    matchCount := 0;
    for match in matches do
    begin
      inc(matchCount);
      if (Memo1.Lines.Count > 0) then
            Memo1.Lines.Add('');
      Memo1.Lines.Add(Format('Match %d:%s Offset: %d Length: %d Value: "%s"',
        [matchCount, sLineBreak, match.Index, match.Length, match.Value]));

      for ix := match.Index to (match.Index + match.Length - 1) do
        resultArray[ix - 1] := cnstMatch;

      groupIx := 1;
      while groupIx < match.Groups.Count do
      begin
        group := match.Groups[groupIx];
        Memo1.Lines.Add
          (Format(' Match group %d: offset: %d length: %d value: %s',
          [groupIx, group.Index, group.Length, group.Value]));
        inc(groupIx);
        for ix := group.Index to (group.Index + group.Length - 1) do
          resultArray[ix - 1] := cnstCaptured;
      end;
    end;

    currentColour := resultArray[0];
    outString := '';
    ix := Low(resultArray) - 1;
    while ix < High(resultArray) do
    begin
      inc(ix);
      if (ix <= High(resultArray) - Length(sLineBreak) + 1) then
      begin
        if (wkText.Substring(ix, Length(sLineBreak)) = sLineBreak) then
        begin
          reResults.AddText(outString, currentColour);
          reResults.AddLineBreak();
          outString := '';
          ix := ix + Length(sLineBreak) - 1;
          continue;
        end;
      end;

      if resultArray[ix] = currentColour then
        outString := outString + wkText.Substring(ix, 1)
      else
      begin
        reResults.AddText(outString, currentColour);
        currentColour := resultArray[ix];
        outString := wkText.Substring(ix, 1);
      end;
    end;
    if outString.Length > 0 then
      reResults.AddText(outString, currentColour);
  finally
    Processing := False;
  end;
end;

procedure TForm1.mmoMatchTextChange(Sender: TObject);
begin
  Timer2.Enabled := False;
  Timer2.Enabled := True;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
  procedure SetOption(var regExOptions: TRegExOptions; checkBox: TCheckBox);
  begin
    if (checkBox.IsChecked) then
      Include(regExOptions, TRegExOption(checkBox.Tag));
  end;

var
  match: TMatch;
  regExOptions: TRegExOptions;
begin
  Timer1.Enabled := False;
  if (Trim(reRegEx.Text).Length > 0) then
  begin
    try
      SetOption(regExOptions, cbxIgnoreCase);
      SetOption(regExOptions, cbxMultiLine);
      SetOption(regExOptions, cbxExplicitCapture);
      SetOption(regExOptions, cbxSingleLine);
      SetOption(regExOptions, cbxIgnorePatternSpace);
      SetOption(regExOptions, cbxNotEmpty);
      SetOption(regExOptions, cbxCompiled);

      RegEx := TRegEx.Create(Trim(reRegEx.Text), regExOptions);
      match := RegEx.match('A');
      lblRegExErr.Visible := False;
    except
      on E: ERegularExpressionError do
      begin
        lblRegExErr.Text := E.Message;
        lblRegExErr.Visible := true;
      end;
    end;
  end;
  ProcessRegExMatch;
end;

procedure TForm1.Timer2Timer(Sender: TObject);
begin
  if Processing then
    exit;
  Timer2.Enabled := False;
  ProcessRegExMatch;
end;

end.
