# Delphi RegEx
This repository contains the slides and code from my presentation to the Canberra Chapter of the Australian Delphi User Group (ADUG) 19th June 2019.

Contents:

1. RegEx Presentation.pdf contains the slides from the presentation.
2. "ADUG RegEx.groupproj" is a Group Project containing three Delphi Projects:

* "CodingSample" is a console application that shows how to match text given a regular expression.

* "RegExVcl" is a utility to demonstrate and test Delphi's implementation of regular expressions. It highlights matches and captured groups in different colours.

* "RegExFmx" is a FireMonkey, cross platform version of RegExVcl

Note that these examples have been created with Delphi 10.3 but should compile with all versions from Delphi XE and up.

RegExFmx uses the TMS FMX Rich Editor component, TTMSFMXRichEditor. If you do not have this installed you could remove it and all references to it.

Good Luck!

Steve