object Form2: TForm2
  Left = 0
  Top = 0
  Caption = ' ADUG RegEx Demo'
  ClientHeight = 629
  ClientWidth = 849
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    849
    629)
  PixelsPerInch = 96
  TextHeight = 18
  object lblRegExErr: TLabel
    Left = 16
    Top = 0
    Width = 71
    Height = 18
    Caption = 'lblRegExErr'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 16
    Top = 25
    Width = 127
    Height = 18
    Caption = '&Regular Expression:'
    FocusControl = reRegEx
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 16
    Top = 215
    Width = 104
    Height = 18
    Caption = '&Text To Match:'
    FocusControl = mmoMatch
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 16
    Top = 423
    Width = 51
    Height = 18
    Caption = '&Results:'
    FocusControl = reResults
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object reRegEx: TMemo
    Left = 16
    Top = 56
    Width = 441
    Height = 153
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 0
    OnChange = reRegExChange
  end
  object Memo1: TMemo
    Left = 480
    Top = 240
    Width = 353
    Height = 369
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object mmoMatch: TMemo
    Left = 16
    Top = 240
    Width = 441
    Height = 177
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 2
    OnChange = mmoMatchChange
  end
  object reResults: TRichEdit
    Left = 16
    Top = 448
    Width = 441
    Height = 161
    Anchors = [akLeft, akTop, akBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 3
    Zoom = 100
  end
  object GroupBox1: TGroupBox
    Left = 480
    Top = 56
    Width = 353
    Height = 153
    Caption = ' Options '
    TabOrder = 4
    object cbxIgnoreCase: TCheckBox
      Left = 32
      Top = 32
      Width = 97
      Height = 20
      Hint = 'Specifies case-insensitive matching.'
      Caption = 'Ignore Case'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = reRegExChange
    end
    object cbxMultiLine: TCheckBox
      Left = 32
      Top = 60
      Width = 97
      Height = 20
      Hint = 
        'Changes the meaning of ^ and $ so they match at the beginning an' +
        'd end, respectively, of any line, and not just the beginning and' +
        ' end of the entire string.'
      Caption = 'Multi-Line'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = reRegExChange
    end
    object cbxExplicitCapture: TCheckBox
      Left = 32
      Top = 88
      Width = 161
      Height = 20
      Caption = 'Explicit Capture'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = reRegExChange
    end
    object cbxSingleLine: TCheckBox
      Left = 32
      Top = 117
      Width = 97
      Height = 20
      Caption = 'Single Line'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = reRegExChange
    end
    object cbxIgnorePatternSpace: TCheckBox
      Left = 160
      Top = 32
      Width = 177
      Height = 20
      Caption = 'Ignore Pattern Space'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = reRegExChange
    end
    object cbxNotEmpty: TCheckBox
      Left = 160
      Top = 60
      Width = 97
      Height = 20
      Hint = 'No idea, it'#39's not documented!~!'
      Caption = 'Not Empty'
      Checked = True
      ParentShowHint = False
      ShowHint = True
      State = cbChecked
      TabOrder = 5
      OnClick = reRegExChange
    end
    object cbxCompiled: TCheckBox
      Left = 160
      Top = 117
      Width = 97
      Height = 20
      Caption = 'Compiled'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      OnClick = reRegExChange
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 488
    Top = 48
  end
  object Timer2: TTimer
    Enabled = False
    OnTimer = Timer2Timer
    Left = 536
    Top = 48
  end
end
